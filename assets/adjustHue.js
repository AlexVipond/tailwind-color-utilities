import { getHue, getSat, getLig } from '~/assets/parse-hsla.js'

export default function adjustHue(hsla, degrees) {
  const hue = getHue(hsla)
  const sat = getSat(hsla)
  const lig = getLig(hsla)
  const adjustedHue = hue + degrees > 360
    ? hue + degrees - 360
    : hue + degrees < 0
      ? hue + degrees + 360
      : hue + degrees

  return `hsla(${adjustedHue}, ${sat}%, ${lig}%, 1.0)`
}
