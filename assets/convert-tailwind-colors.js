const fs = require('fs')
const convert = require('color-convert')

function convertColor(fromFormat, toFormat, color) {
  return convert[fromFormat][toFormat](color)
}

function formatHsla(hslOutput) {
  return `hsla(${hslOutput[0]}, ${hslOutput[1]}%, ${hslOutput[2]}%, 1.0)`
}

function addHsla(palette) {
  return Object.keys(palette).reduce((newColors, color) => {
    return {
      ...newColors,
      [color]: Object.keys(palette[color]).reduce((newShades, shade) => {
        return {
          ...newShades,
          [shade]: {
            ...palette[color][shade],
            hsla: formatHsla(convertColor('hex', 'hsl', palette[color][shade].hex.slice(1)))
          }
        }
      }, {})
    }
  }, {})
}

const palette = JSON.parse(fs.readFileSync(
  './tailwind-palette.json',
  'utf8'
))
const newColors = addHsla(palette)

fs.writeFileSync(
  './tailwind-palette.json',
  JSON.stringify(newColors, null, 2)
)
