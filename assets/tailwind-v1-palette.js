export default {
  'gray': {
    '100': {
      'hex': '#f7fafc',
      'hsla': 'hsla(204, 45%, 98%, 1.0)'
    },
    '200': {
      'hex': '#edf2f7',
      'hsla': 'hsla(210, 38%, 95%, 1.0)'
    },
    '300': {
      'hex': '#e2e8f0',
      'hsla': 'hsla(214, 32%, 91%, 1.0)'
    },
    '400': {
      'hex': '#cbd5e0',
      'hsla': 'hsla(211, 25%, 84%, 1.0)'
    },
    '500': {
      'hex': '#a0aec0',
      'hsla': 'hsla(214, 20%, 69%, 1.0)'
    },
    '600': {
      'hex': '#718096',
      'hsla': 'hsla(216, 15%, 52%, 1.0)'
    },
    '700': {
      'hex': '#4a5568',
      'hsla': 'hsla(218, 17%, 35%, 1.0)'
    },
    '800': {
      'hex': '#2d3748',
      'hsla': 'hsla(218, 23%, 23%, 1.0)'
    },
    '900': {
      'hex': '#1a202c',
      'hsla': 'hsla(220, 26%, 14%, 1.0)'
    },
    '1000': {
      'hex': '#0F141C',
      'hsla': 'hsla(217, 30%, 8%, 1.0)'
    }
  },
  'red': {
    '100': {
      'hex': '#fff5f5',
      'hsla': 'hsla(0, 100%, 98%, 1.0)'
    },
    '200': {
      'hex': '#fed7d7',
      'hsla': 'hsla(0, 95%, 92%, 1.0)'
    },
    '300': {
      'hex': '#feb2b2',
      'hsla': 'hsla(0, 97%, 85%, 1.0)'
    },
    '400': {
      'hex': '#fc8181',
      'hsla': 'hsla(0, 95%, 75%, 1.0)'
    },
    '500': {
      'hex': '#f56565',
      'hsla': 'hsla(0, 88%, 68%, 1.0)'
    },
    '600': {
      'hex': '#e53e3e',
      'hsla': 'hsla(0, 76%, 57%, 1.0)'
    },
    '700': {
      'hex': '#c53030',
      'hsla': 'hsla(0, 61%, 48%, 1.0)'
    },
    '800': {
      'hex': '#9b2c2c',
      'hsla': 'hsla(0, 56%, 39%, 1.0)'
    },
    '900': {
      'hex': '#742a2a',
      'hsla': 'hsla(0, 47%, 31%, 1.0)'
    }
  },
  'orange': {
    '100': {
      'hex': '#fffaf0',
      'hsla': 'hsla(40, 100%, 97%, 1.0)'
    },
    '200': {
      'hex': '#feebc8',
      'hsla': 'hsla(39, 96%, 89%, 1.0)'
    },
    '300': {
      'hex': '#fbd38d',
      'hsla': 'hsla(38, 93%, 77%, 1.0)'
    },
    '400': {
      'hex': '#f6ad55',
      'hsla': 'hsla(33, 90%, 65%, 1.0)'
    },
    '500': {
      'hex': '#ed8936',
      'hsla': 'hsla(27, 84%, 57%, 1.0)'
    },
    '600': {
      'hex': '#dd6b20',
      'hsla': 'hsla(24, 75%, 50%, 1.0)'
    },
    '700': {
      'hex': '#c05621',
      'hsla': 'hsla(20, 71%, 44%, 1.0)'
    },
    '800': {
      'hex': '#9c4221',
      'hsla': 'hsla(16, 65%, 37%, 1.0)'
    },
    '900': {
      'hex': '#7b341e',
      'hsla': 'hsla(14, 61%, 30%, 1.0)'
    }
  },
  'yellow': {
    '100': {
      'hex': '#fffff0',
      'hsla': 'hsla(60, 100%, 97%, 1.0)'
    },
    '200': {
      'hex': '#fefcbf',
      'hsla': 'hsla(58, 97%, 87%, 1.0)'
    },
    '300': {
      'hex': '#faf089',
      'hsla': 'hsla(55, 92%, 76%, 1.0)'
    },
    '400': {
      'hex': '#f6e05e',
      'hsla': 'hsla(51, 89%, 67%, 1.0)'
    },
    '500': {
      'hex': '#ecc94b',
      'hsla': 'hsla(47, 81%, 61%, 1.0)'
    },
    '600': {
      'hex': '#d69e2e',
      'hsla': 'hsla(40, 67%, 51%, 1.0)'
    },
    '700': {
      'hex': '#b7791f',
      'hsla': 'hsla(36, 71%, 42%, 1.0)'
    },
    '800': {
      'hex': '#975a16',
      'hsla': 'hsla(32, 75%, 34%, 1.0)'
    },
    '900': {
      'hex': '#744210',
      'hsla': 'hsla(30, 76%, 26%, 1.0)'
    }
  },
  'green': {
    '100': {
      'hex': '#f0fff4',
      'hsla': 'hsla(136, 100%, 97%, 1.0)'
    },
    '200': {
      'hex': '#c6f6d5',
      'hsla': 'hsla(139, 73%, 87%, 1.0)'
    },
    '300': {
      'hex': '#9ae6b4',
      'hsla': 'hsla(141, 60%, 75%, 1.0)'
    },
    '400': {
      'hex': '#68d391',
      'hsla': 'hsla(143, 55%, 62%, 1.0)'
    },
    '500': {
      'hex': '#48bb78',
      'hsla': 'hsla(145, 46%, 51%, 1.0)'
    },
    '600': {
      'hex': '#38a169',
      'hsla': 'hsla(148, 48%, 43%, 1.0)'
    },
    '700': {
      'hex': '#2f855a',
      'hsla': 'hsla(150, 48%, 35%, 1.0)'
    },
    '800': {
      'hex': '#276749',
      'hsla': 'hsla(152, 45%, 28%, 1.0)'
    },
    '900': {
      'hex': '#22543d',
      'hsla': 'hsla(152, 42%, 23%, 1.0)'
    }
  },
  'teal': {
    '100': {
      'hex': '#e6fffa',
      'hsla': 'hsla(168, 100%, 95%, 1.0)'
    },
    '200': {
      'hex': '#b2f5ea',
      'hsla': 'hsla(170, 77%, 83%, 1.0)'
    },
    '300': {
      'hex': '#81e6d9',
      'hsla': 'hsla(172, 67%, 70%, 1.0)'
    },
    '400': {
      'hex': '#4fd1c5',
      'hsla': 'hsla(174, 59%, 56%, 1.0)'
    },
    '500': {
      'hex': '#38b2ac',
      'hsla': 'hsla(177, 52%, 46%, 1.0)'
    },
    '600': {
      'hex': '#319795',
      'hsla': 'hsla(179, 51%, 39%, 1.0)'
    },
    '700': {
      'hex': '#2c7a7b',
      'hsla': 'hsla(181, 47%, 33%, 1.0)'
    },
    '800': {
      'hex': '#285e61',
      'hsla': 'hsla(183, 42%, 27%, 1.0)'
    },
    '900': {
      'hex': '#234e52',
      'hsla': 'hsla(185, 40%, 23%, 1.0)'
    },
    '1000': {
      'hex': '#1E4145',
      'hsla': 'hsla(186, 40%, 19%, 1.0)'
    }
  },
  'blue': {
    '100': {
      'hex': '#ebf8ff',
      'hsla': 'hsla(201, 100%, 96%, 1.0)'
    },
    '200': {
      'hex': '#bee3f8',
      'hsla': 'hsla(202, 81%, 86%, 1.0)'
    },
    '300': {
      'hex': '#90cdf4',
      'hsla': 'hsla(203, 82%, 76%, 1.0)'
    },
    '400': {
      'hex': '#63b3ed',
      'hsla': 'hsla(205, 79%, 66%, 1.0)'
    },
    '500': {
      'hex': '#4299e1',
      'hsla': 'hsla(207, 73%, 57%, 1.0)'
    },
    '600': {
      'hex': '#3182ce',
      'hsla': 'hsla(209, 62%, 50%, 1.0)'
    },
    '700': {
      'hex': '#2b6cb0',
      'hsla': 'hsla(211, 61%, 43%, 1.0)'
    },
    '800': {
      'hex': '#2c5282',
      'hsla': 'hsla(213, 49%, 34%, 1.0)'
    },
    '900': {
      'hex': '#2a4365',
      'hsla': 'hsla(215, 41%, 28%, 1.0)'
    },
    '1000': {
      'hex': '#26364D',
      'hsla': 'hsla(215, 34%, 23%, 1.0)'
    }
  },
  'indigo': {
    '100': {
      'hex': '#ebf4ff',
      'hsla': 'hsla(213, 100%, 96%, 1.0)'
    },
    '200': {
      'hex': '#c3dafe',
      'hsla': 'hsla(217, 97%, 88%, 1.0)'
    },
    '300': {
      'hex': '#a3bffa',
      'hsla': 'hsla(221, 90%, 81%, 1.0)'
    },
    '400': {
      'hex': '#7f9cf5',
      'hsla': 'hsla(225, 86%, 73%, 1.0)'
    },
    '500': {
      'hex': '#667eea',
      'hsla': 'hsla(229, 76%, 66%, 1.0)'
    },
    '600': {
      'hex': '#5a67d8',
      'hsla': 'hsla(234, 62%, 60%, 1.0)'
    },
    '700': {
      'hex': '#4c51bf',
      'hsla': 'hsla(237, 47%, 52%, 1.0)'
    },
    '800': {
      'hex': '#434190',
      'hsla': 'hsla(242, 38%, 41%, 1.0)'
    },
    '900': {
      'hex': '#3c366b',
      'hsla': 'hsla(247, 33%, 32%, 1.0)'
    }
  },
  'purple': {
    '100': {
      'hex': '#faf5ff',
      'hsla': 'hsla(270, 100%, 98%, 1.0)'
    },
    '200': {
      'hex': '#e9d8fd',
      'hsla': 'hsla(268, 90%, 92%, 1.0)'
    },
    '300': {
      'hex': '#d6bcfa',
      'hsla': 'hsla(265, 86%, 86%, 1.0)'
    },
    '400': {
      'hex': '#b794f4',
      'hsla': 'hsla(262, 81%, 77%, 1.0)'
    },
    '500': {
      'hex': '#9f7aea',
      'hsla': 'hsla(260, 73%, 70%, 1.0)'
    },
    '600': {
      'hex': '#805ad5',
      'hsla': 'hsla(259, 59%, 59%, 1.0)'
    },
    '700': {
      'hex': '#6b46c1',
      'hsla': 'hsla(258, 50%, 52%, 1.0)'
    },
    '800': {
      'hex': '#553c9a',
      'hsla': 'hsla(256, 44%, 42%, 1.0)'
    },
    '900': {
      'hex': '#44337a',
      'hsla': 'hsla(254, 41%, 34%, 1.0)'
    }
  },
  'pink': {
    '100': {
      'hex': '#fff5f7',
      'hsla': 'hsla(348, 100%, 98%, 1.0)'
    },
    '200': {
      'hex': '#fed7e2',
      'hsla': 'hsla(343, 95%, 92%, 1.0)'
    },
    '300': {
      'hex': '#fbb6ce',
      'hsla': 'hsla(339, 90%, 85%, 1.0)'
    },
    '400': {
      'hex': '#f687b3',
      'hsla': 'hsla(336, 86%, 75%, 1.0)'
    },
    '500': {
      'hex': '#ed64a6',
      'hsla': 'hsla(331, 79%, 66%, 1.0)'
    },
    '600': {
      'hex': '#d53f8c',
      'hsla': 'hsla(329, 64%, 54%, 1.0)'
    },
    '700': {
      'hex': '#b83280',
      'hsla': 'hsla(325, 57%, 46%, 1.0)'
    },
    '800': {
      'hex': '#97266d',
      'hsla': 'hsla(322, 60%, 37%, 1.0)'
    },
    '900': {
      'hex': '#702459',
      'hsla': 'hsla(318, 51%, 29%, 1.0)'
    }
  }
}
