const { execSync } = require('child_process')

module.exports = function (palette) {
  const command = `npx babel assets/${palette}.js --out-dir lib`
  execSync(command, (err, stdout, stderr) => {
    if (err) {
      // node couldn't execute the command
      return
    }

    // the *entire* stdout and stderr (buffered)
    console.log(`stdout: ${stdout}`)
    console.log(`stderr: ${stderr}`)
  })
}
