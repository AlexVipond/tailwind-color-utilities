module.exports = {
  theme: {
    fractions: {
      '1/2': '50%',
      '1/3': '33.33333%',
      '2/3': '66.66667%',
      '1/4': '25%',
      '2/4': '50%',
      '3/4': '75%',
      '1/5': '20%',
      '2/5': '40%',
      '3/5': '60%',
      '4/5': '80%',
      '1/6': '16.66667%',
      '2/6': '33.33333%',
      '3/6': '50%',
      '4/6': '66.66667%',
      '5/6': '83.33333%',
      '1/12': '8.33333%',
      '2/12': '16.66667%',
      '3/12': '25%',
      '4/12': '33.33333%',
      '5/12': '41.66667%',
      '6/12': '50%',
      '7/12': '58.33333%',
      '8/12': '66.66667%',
      '9/12': '75%',
      '10/12': '83.33333%',
      '11/12': '91.66667%',
      'full': '100%'
    },
    screenWidthFractions: {
      '1/2': 'calc(1/2 * 100vw)',
      '1/3': 'calc(1/3 * 100vw)',
      '2/3': 'calc(2/3 * 100vw)',
      '1/4': 'calc(1/4 * 100vw)',
      '2/4': 'calc(2/4 * 100vw)',
      '3/4': 'calc(3/4 * 100vw)',
      '1/5': 'calc(1/5 * 100vw)',
      '2/5': 'calc(2/5 * 100vw)',
      '3/5': 'calc(3/5 * 100vw)',
      '4/5': 'calc(4/5 * 100vw)',
      '1/6': 'calc(1/6 * 100vw)',
      '2/6': 'calc(2/6 * 100vw)',
      '3/6': 'calc(3/6 * 100vw)',
      '4/6': 'calc(4/6 * 100vw)',
      '5/6': 'calc(5/6 * 100vw)',
      '1/12': 'calc(1/12 * 100vw)',
      '2/12': 'calc(2/12 * 100vw)',
      '3/12': 'calc(3/12 * 100vw)',
      '4/12': 'calc(4/12 * 100vw)',
      '5/12': 'calc(5/12 * 100vw)',
      '6/12': 'calc(6/12 * 100vw)',
      '7/12': 'calc(7/12 * 100vw)',
      '8/12': 'calc(8/12 * 100vw)',
      '9/12': 'calc(9/12 * 100vw)',
      '10/12': 'calc(10/12 * 100vw)',
      '11/12': 'calc(11/12 * 100vw)',
      'screen': '100vw'
    },
    screenHeightFractions: {
      '1/2': 'calc(1/2 * 100vh)',
      '1/3': 'calc(1/3 * 100vh)',
      '2/3': 'calc(2/3 * 100vh)',
      '1/4': 'calc(1/4 * 100vh)',
      '2/4': 'calc(2/4 * 100vh)',
      '3/4': 'calc(3/4 * 100vh)',
      '1/5': 'calc(1/5 * 100vh)',
      '2/5': 'calc(2/5 * 100vh)',
      '3/5': 'calc(3/5 * 100vh)',
      '4/5': 'calc(4/5 * 100vh)',
      '1/6': 'calc(1/6 * 100vh)',
      '2/6': 'calc(2/6 * 100vh)',
      '3/6': 'calc(3/6 * 100vh)',
      '4/6': 'calc(4/6 * 100vh)',
      '5/6': 'calc(5/6 * 100vh)',
      '1/12': 'calc(1/12 * 100vh)',
      '2/12': 'calc(2/12 * 100vh)',
      '3/12': 'calc(3/12 * 100vh)',
      '4/12': 'calc(4/12 * 100vh)',
      '5/12': 'calc(5/12 * 100vh)',
      '6/12': 'calc(6/12 * 100vh)',
      '7/12': 'calc(7/12 * 100vh)',
      '8/12': 'calc(8/12 * 100vh)',
      '9/12': 'calc(9/12 * 100vh)',
      '10/12': 'calc(10/12 * 100vh)',
      '11/12': 'calc(11/12 * 100vh)',
      'screen': '100vh'
    },
    screens: {
      sm: '640px',
      md: '768px',
      lg: '1024px',
      xl: '1280px'
    },
    spacing: {
      px: '1px',
      '0': '0',
      '1': '0.25rem',
      '2': '0.5rem',
      '3': '0.75rem',
      '4': '1rem',
      '5': '1.25rem',
      '6': '1.5rem',
      '7': '2rem',
      '8': '2.5rem',
      '9': '3rem',
      '10': '4rem',
      '11': '5rem',
      '12': '6rem',
      '13': '8rem',
      '14': '10rem',
      '15': '12rem',
      '16': '14rem',
      '17': '16rem'
    },
    borderRadius: {
      '0': '0',
      default: '0.125rem',
      '1': '0.25rem',
      '2': '0.5rem',
      full: '9999px'
    },
    borderWidth: {
      '0': '0',
      default: '1px',
      '1': '1.5px',
      '2': '2px',
      '3': '4px',
      '4': '8px'
    },
    boxShadow: {
      '0': 'none',
      default: '0 1px 3px 0 rgba(0, 0, 0, 0.1), 0 1px 2px 0 rgba(0, 0, 0, 0.06)',
      '1': '0 4px 6px -1px rgba(0, 0, 0, 0.1), 0 2px 4px -1px rgba(0, 0, 0, 0.06)',
      '2': '0 10px 15px -3px rgba(0, 0, 0, 0.1), 0 4px 6px -2px rgba(0, 0, 0, 0.05)',
      '3': '0 20px 25px -5px rgba(0, 0, 0, 0.1), 0 10px 10px -5px rgba(0, 0, 0, 0.04)',
      '4': '0 25px 50px -12px rgba(0, 0, 0, 0.25)',
      '-1': 'inset 0 2px 4px 0 rgba(0, 0, 0, 0.06)',
      outline: '0 0 0 3px rgba(66, 153, 225, 0.5)'
    },
    fontSize: {
      '1': '0.75rem',
      '2': '0.875rem',
      '3': '1rem',
      '4': '1.125rem',
      '5': '1.25rem',
      '6': '1.5rem',
      '7': '1.875rem',
      '8': '2.25rem',
      '9': '3rem',
      '10': '4rem'
    },
    fontWeight: {
      '100': '100',
      '200': '200',
      '300': '300',
      '400': '400',
      '500': '500',
      '600': '600',
      '700': '700',
      '800': '800',
      '900': '900'
    },
    height: theme => ({
      auto: 'auto',
      ...theme('spacing'),
      ...theme('fractions'),
      ...theme('screenHeightFractions')
    }),
    letterSpacing: {
      '-2': '-0.05em',
      '-1': '-0.025em',
      '0': '0',
      '1': '0.025em',
      '2': '0.05em',
      '3': '0.1em'
    },
    lineHeight: {
      '0': '1',
      '1': '1.25',
      '2': '1.375',
      '3': '1.5',
      '4': '1.625',
      '5': '2'
    },
    margin: (theme, { negative }) => ({
      auto: 'auto',
      ...theme('spacing'),
      ...negative(theme('spacing'))
    }),
    maxHeight: theme => ({
      ...theme('fractions'),
      ...theme('screenHeightFractions')
    }),
    maxWidth: theme => ({
      '1': '20rem',
      '2': '24rem',
      '3': '28rem',
      '4': '32rem',
      '5': '36rem',
      '6': '42rem',
      '7': '48rem',
      '8': '56rem',
      '9': '64rem',
      '10': '72rem',
      ...theme('fractions'),
      ...theme('screenWidthFractions')
    }),
    minHeight: theme => ({
      '0': '0',
      ...theme('fractions'),
      ...theme('screenHeightFractions')
    }),
    minWidth: theme => ({
      '0': '0',
      ...theme('fractions'),
      ...theme('screenWidthFractions')
    }),
    width: theme => ({
      auto: 'auto',
      ...theme('spacing'),
      ...theme('fractions'),
      ...theme('screenWidthFractions')
    }),
    inset: (theme, { negative }) => ({
      auto: 'auto',
      ...theme('spacing'),
      ...negative(theme('spacing'))
    })
  },
  variants: ['responsive', 'group-hover', 'focus-within', 'hover', 'focus', 'active']
}
