const config = {
  paths: [],
  whitelist: [
    'bg-gray-200',
    'antialiased',
    'font-sans',
    'overflow-x-hidden'
  ]
}

export default config
