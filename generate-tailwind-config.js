const fs = require('fs')
const { getTailwindConfig } = require('./scripts/get-tailwind-config')
const alPalette = require(`./assets/al-palette.json`)
const tailwindPalette = require(`./assets/tailwind-palette.json`)

function toTailwindConfig(palette) {
  return Object.keys(palette).reduce((tailwindConfig, color) => ({ ...tailwindConfig, [color]: getTailwindConfig(palette[color], color) }), {})
}

function generateTailwindConfig() {
  console.log('Generating Tailwind config...')
  const palettes = [alPalette, tailwindPalette]
  const tailwindConfig = palettes.reduce((palettteAppImport, palette) => palettteAppImport.concat(toTailwindConfig(palette)), [])

  fs.writeFileSync(
    './assets/tailwind-config.json',
    JSON.stringify(tailwindConfig, null, 2)
  )
  console.log('Tailwind config generated successfully!')
}

module.exports = {
  generateTailwindConfig: generateTailwindConfig
}
