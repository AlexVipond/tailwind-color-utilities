export default function getTailwindConfig(color) {
  const shades = Object.keys(color)
  return shades.reduce((config, shade) => ({ ...config, [shade]: color[shade].hsla }), {})
}
