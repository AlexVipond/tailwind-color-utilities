const fs = require('fs')
const { getPalettteAppImport } = require('./scripts/get-palettte-app-import')

function parsePalette(filePath) {
  return JSON.parse(
    fs.readFileSync(filePath, 'utf8')
      .replace(/(?:export default |\/\/.*?\n)/g, '')
      .replace(/'/g, '"')
  )
}

const alPalette = parsePalette(`./assets/al-palette.js`)
const tailwindV1Palette = parsePalette(`./assets/tailwind-v1-palette.js`)
const tailwindV2Palette = parsePalette(`./assets/tailwind-v2-palette.js`)

function generatePalettteAppImport() {
  console.log('Generating Palettte App import...')
  const palettes = [alPalette, tailwindV1Palette, tailwindV2Palette]
  const palettteAppImport = palettes.reduce((palettteAppImport, palette) => palettteAppImport.concat(getPalettteAppImport(palette)), [])

  fs.writeFileSync(
    './assets/palettte-app-import.json',
    JSON.stringify(palettteAppImport, null, 2)
  )
  console.log('Palettte App import generated successfully!')
}

module.exports = {
  generatePalettteAppImport: generatePalettteAppImport
}
