const { writeFileSync } = require('fs')
const chroma = require('chroma-js')
const hexes = require('./tailwind-v2-input.js')
const colors = Object.keys(hexes)
const withHsla = colors.reduce((withHsla, color) => {
  return {
    ...withHsla,
    [color]: Object.keys(hexes[color]).reduce((shades, shade) => {
      const hex = hexes[color][shade]
      const { 0: h, 1: s, 2: l } = chroma(hex).hsl()
      if (color === 'trueGray') {
        console.log(isNaN(h) ? 0 : h)
        console.log(Math.round(isNaN(h) ? 0 : h))
      }
      return {
        ...shades,
        [shade]: {
          hex,
          hsla: `hsla(${Math.round(isNaN(h) ? 0 : h)}, ${Math.round(s * 100)}%, ${Math.round(l * 100)}%, 1.0)`
        }
      }
    }, {})
  }
}, {})

writeFileSync('./tailwind-v2-output.json', JSON.stringify(withHsla, null, 2))
