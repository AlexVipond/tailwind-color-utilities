import { getHue, getSat, getLig } from '~/assets/parse-hsla.js'

export default function adjustSaturationOrLightness(hsla, multiplier, mode) {
  const hue = getHue(hsla)
  const sat = getSat(hsla)
  const lig = getLig(hsla)
  const adjusted = mode === 'sat'
    ? Math.min(Math.round(sat * multiplier), 100)
    : mode === 'lig'
      ? Math.min(Math.round(lig * multiplier), 100)
      : null

  return mode === 'sat'
    ? `hsla(${hue}, ${adjusted}%, ${lig}%, 1.0)`
    : `hsla(${hue}, ${sat}%, ${adjusted}%, 1.0)`
}
