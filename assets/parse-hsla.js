function getHue(hsla) {
  return Number(hsla.replace('hsla(', '').split(',')[0].replace(/%/, ''))
}

function getSat(hsla) {
  return Number(hsla.replace('hsla(', '').split(',')[1].replace(/%/, ''))
}

function getLig(hsla) {
  return Number(hsla.replace('hsla(', '').split(',')[2].replace(/%/, ''))
}

export {
  getHue,
  getSat,
  getLig
}
