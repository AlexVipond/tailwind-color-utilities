import convert from 'color-convert'
import { getHue, getSat, getLig } from './parse-hsla.js'

function getDimensions(hsla) {
  return [
    getHue(hsla),
    getSat(hsla),
    getLig(hsla)
  ]
}

export default function getPalettteAppImport(palette) {
  const colors = Object.keys(palette)
  return colors.reduce((palettteAppImport, color) => {
    const shades = Object.keys(palette[color])
    return palettteAppImport.concat({
      paletteName: color,
      swatches: shades.reduce((swatches, shade) => {
        return swatches.concat({
          name: shade,
          color: convert.hsl.hex(...getDimensions(palette[color][shade].hsla))
        })
      }, [])
    })
  }, [])
}
