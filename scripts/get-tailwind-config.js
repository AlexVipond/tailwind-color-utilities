function getTailwindConfig(color, colorName) {
  const shades = Object.keys(color)
  return shades.reduce((config, shade) => ({ ...config, [shade]: color[shade].hsla }), {})
}

module.exports = {
  getTailwindConfig
}
